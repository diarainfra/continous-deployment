var mongoose = require('mongoose');


var projectSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    projectname: {
        type: String,
        unique: true,
        required: true
    },
    productowner: {
        type: String,
        required: true
    },
    pivopid: {
        type: String
    },
    sshhost: {
        type: String,
        required: true
    },
    dbname: {
        type: String,
        required: true
    },
    dbusername: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now()
    }
});

var Project = module.exports = mongoose.model('Project', projectSchema);

module.exports.getProjectById = function (id, callback) {
    Project.findById(id, callback);
};

module.exports.getAllProjects = function (callback) {
    Project.find(callback);
};
