var mongoose = require('mongoose');


var serverSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    servername: {
        type: String,
        unique: true,
        required:true
    },
    serverhost: {
        type: String,
        unique: true,
        required:true
    },
    serverusername: {
        type: String,
        required:true
    },
    serverpassword: {
        type: String,
        required:true
    },
    bitbucketdeploykey: {
    type: String,
        required:true
}
});

var Server = module.exports = mongoose.model('Server', serverSchema);

module.exports.getServerById = function (id, callback) {
    Server.findById(id, callback);
};


module.exports.getAllServers = function (callback) {
    Server.find(callback);
};
