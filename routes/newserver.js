const express = require('express'); // Web Server package
var router = express.Router(); // Router
const mongoose = require('mongoose'); // Database package
const {ensureAuthenticated, forwardAuthenticated} = require('../config/auth');
const bcrypt = require('bcryptjs');


//Database
db = require('../db');
const Server = require('../models/servers');

// New Server

router.post('/', ensureAuthenticated, (req, res) => {
    const server = new Server({
        _id: new mongoose.Types.ObjectId(),
        servername: req.body.servername,
        serverhost: req.body.serverhost,
        serverusername: req.body.serverusername,
        serverpassword: req.body.serverpassword
    });

    //Server save to MongoDB database
    /*server
        .save()
        .then(result => {
            console.log('Sending data to database');
            console.log(result)
        })
        .catch(err => console.log(err));

    //Redirect to Projects page
    res.writeHead(301,
        {Location: '/projects/'}
    );
    res.end();*/
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(server.serverpassword, salt, (err, hash) => {
            if (err) throw err;
            server.serverpassword = hash;
            server
                .save()
                .then(result => {
                    console.log('Sending data to database');
                    console.log(result)
                })
                .catch(err => console.log(err));

            //Redirect to Projects page
            res.writeHead(301,
                {Location: '/projects/'}
            );
            res.end();
        });
    });

});

module.exports = router;
