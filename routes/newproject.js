const express = require('express'); // Web Server package
var router = express.Router(); // Router
const mongoose = require('mongoose'); // Database package
const fetch = require('node-fetch'); // http requests
const config = require('../config'); // config
const Client = require('ssh2').Client; // SSH connection package to create database and folder
const fs = require('fs-extra'); // To read SSH privatekey
const jsonfile = require('jsonfile');
const dotenv = require('dotenv');
dotenv.config();


const {ensureAuthenticated, forwardAuthenticated} = require('../config/auth');



//Database
const Server = require('../models/servers');
const Project = require('../models/projects');
db = require('../db');

// define the home page route
router.get('/', ensureAuthenticated, (req, res) => {
    Server.find(function (err, servers) {
        if (err) {
            console.log(err);
        } else {
            res.set('Content-Type', 'text/html');
            res.render('newproject', {page: 'New Project', servers: servers});
        }
    });
});

/*// define the home page route
router.get('/', ensureAuthenticated, (req, res) => {
    res.render('newproject', {page: 'New Project'})
});*/

// New Project
router.post('/', ensureAuthenticated, (req, res) => {
    const project = new Project({
        _id: new mongoose.Types.ObjectId(),
        projectname: req.body.projectname,
        productowner: req.body.productowner,
        sshhost: req.body.sshhost,
        dbname: req.body.dbname,
        dbusername: req.body.dbusername,
        dbpassword: req.body.dbpassword,
        bitpublic: req.body.bitpublic,
        invmember: req.body.invmember
    });
    //PivotalTracker project creation

    fetch('https://www.pivotaltracker.com/services/v5/projects?token=' + process.env.pivotoken, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            name: req.body.projectname,
            public: true
        })
    }).then(res => res.json()
        .then((data) => {
            project.save(function (err) {
                //Adding Pivotal Tracker ID where is new project name
                Project.updateOne({projectname: req.body.projectname}, {pivopid: data.id}, function (err, rs) {
                    console.log(rs);
                    {
                        //Sending Invite to Product Owner to access Pivotal Tracker as Member
                        fetch('https://www.pivotaltracker.com/services/v5/projects/' + data.id + '/memberships?token=' + process.env.pivotoken, {
                            method: 'POST',
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({
                                email: req.body.productowner,
                                role: "member"
                            })
                        })
                    }
                })
            })
        })
    );

    //Bitbucket Repository creation
    fetch('https://api.bitbucket.org/2.0/repositories/' + process.env.bitteam + '/' + req.body.projectname, {
        method: 'POST',
        headers: {
            'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64')
        }
    })

        .then(() => { //Send email invite to get access to repository
            fetch('https://api.bitbucket.org/1.0/invitations/' + process.env.bitteam + '/' + project.projectname + '/', {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64')
                },
                body: JSON.stringify({
                    email: req.body.invmember,
                    permission: "read"
                }),
                timeout: 1000,
            });
            /*fetch('https://api.bitbucket.org/1.0/invitations/' + process.env.bitteam + '/' + req.body.projectname + '/' + req.body.invmember, {
                method: 'POST',
                headers: {
                    'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64')
                },
                body: form
            })*/
        })
        .then(() => {  //Create access-key for repository
            fetch('https://api.bitbucket.org/2.0/repositories/' + process.env.bitteam + '/' + req.body.projectname + '/deploy-keys', {
                method: 'POST',
                headers: {
                    'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64'),
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    key: req.body.bitpublic,
                    label: req.body.projectname + '-key'
                })
            })
        })
        .then(() => {
            fetch('https://api.bitbucket.org/2.0/repositories/' + process.env.bitteam + '/' + req.body.projectname + '/hooks', {
                method: 'POST',
                headers: {
                    'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64')
                },
                body: JSON.stringify({
                    description: req.body.projectname + ' Deploy Hook',
                    url: "http://" + process.env.webhookurl + '/' + project.id + '/deploy',
                    active: true,
                    events: [
                        "repo:push"
                    ]
                })
            })
        })
        .catch(error => console.error(error));

    // create project configuration file
    const file = 'data/' + req.body.projectname + '.json';
    const obj = {
        "prod": {
            "server": {
                "host": req.body.sshhost,
                "user": req.query.Server,
                "password": "Tulineserver1",
                "private_key": "",
                "location": "/home/cdae/projects/" + req.body.projectname,
                "simlink": "/home/cdae/public_html/" + req.body.projectname,
                "user_group": "",
                "version_limit": 3
            },
            "repository": {
                "host": "git@bitbucket.org:" + process.env.bitteam + "/" + req.body.projectname + ".git",
                "branch": "master"
            },
            "command": {
                "post": {
                    "Command_name":
                        "printf '<?php \ndefine('DATABASE_HOSTNAME', 'localhost'); \ndefine('DATABASE_DATABASE', '" + req.body.dbname + "'); \ndefine('DATABASE_USERNAME', '" + req.body.dbusername + "'); \ndefine('DATABASE_PASSWORD', '" + req.body.dbpassword + "');' >  {dir}/config.php && " +
                        "mysql -u cdae " + req.body.dbname + " < {dir}/doc/" + req.body.dbname + ".sql &&" +
                        "cd {dir} && " +
                        "composer install"
                }
            }
        }
    };

    jsonfile.writeFile(file, obj, function (err) {
        if (err) console.error(err)
    });

    //Create Project Folder and Database
    var conn = new Client();
    conn.on('ready', function () {
        console.log('Client :: ready');
        conn.exec('mkdir projects/' + req.body.projectname + ' && echo  Your project ' + req.body.projectname + ' is created > projects/' + req.body.projectname + '/index.html && ln -s ~/projects/' + req.body.projectname + ' ~/public_html', function (err, stream) {
            /*conn.exec('mysql -v -e"create database ' + req.body.dbname + ';grant all on ' + req.body.dbname + '.* to ' + req.body.dbusername +
                '@localhost identified by \'' + req.body.dbpassword + '\' "', function (err, stream)*/
            {
                if (err) throw err;
                stream.on('close', function (code, signal) {
                    console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                    conn.end();
                }).on('data', function (data) {
                    console.log('STDOUT: ' + data);
                }).stderr.on('data', function (data) {
                    console.log('STDERR: ' + data);
                });
            }
        });


    }).connect({
        host: req.body.sshhost,
        port: 22,
        username: 'cdae',
        privateKey: require('fs-extra').readFileSync('.ssh/id_rsa')
    });

    //Project save to MongoDB database
    project
        .save()
        .then(result => {
            console.log('Sending data to database');
            console.log(result)
        })
        .catch(err => console.log(err));

    //Redirect to Projects page
    res.writeHead(301,
        {Location: '/projects/'}
    );
    res.end();
});
module.exports = router;
