const express = require('express');
const router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');

// Welcome Page
router.get('/', forwardAuthenticated, (req, res) => res.render('login', {page:'Login'}));

router.get('/users', forwardAuthenticated, (req, res) => res.render('login', {page:'Login'}));

// Home Page

router.get('/', ensureAuthenticated, (req, res) =>
  res.render('home', { page:'Home'})

);

module.exports = router;
