const express = require('express');
const router = express.Router();
db = require('../db');
const mongoose = require('mongoose');
const Project = require('../models/projects');
const {ensureAuthenticated, forwardAuthenticated} = require('../config/auth');
const execSh = require('exec-sh'); // Execute command line tasks
const fetch = require('node-fetch'); // To send http requests
const nodemailer = require("nodemailer"); // Mail Sender
const Client = require('ssh2').Client; // SSH connection package to create database and folder
const FormData = require('form-data');
const dotenv = require('dotenv');
dotenv.config();

const Server = require('../models/servers');

// define the home page route
router.get('/' , ensureAuthenticated, (req, res) => {
    Project.find(function (err, projects) {
        if (err) {
            console.log(err);
        } else {
            res.set('Content-Type', 'text/html');
            res.render('projects', {page: 'Projects', projects: projects});
        }
    });
});


/*router.get('/servers'/!*, ensureAuthenticated*!/, (req, res) => {
    Server.getServerById('5d00fce54c1c625c54e29ddc',function (err, servers) {
        if (err) {
            console.log(err);
        } else {
            res.json(servers);
        }
    });
});*/


router.get('/:id'  ,ensureAuthenticated);

/*router.get('/:id/view'/!*,ensureAuthenticated*!/, (req, res) => {
    Project.getProjectById(req.params.id, function (err, project) {
        if (err) {
            throw err;
        }
        res.set('Content-Type', 'text/html');
        res.render('view', {page: 'Project view', projects: project})

        /!*fetch('https://www.pivotaltracker.com/services/v5/projects/' + project.pivopid + '/memberships?token=' + process.env.pivotoken, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({})
        })*!/
    })

});*/
router.get('/:id/view' ,ensureAuthenticated, (req, res) => {
    Project.getProjectById(req.params.id, function (err, project) {
        if (err) {
            throw err;
        }
        res.set('Content-Type', 'text/html');
        /*res.render('view', {page: 'Project view', projects: project})*/

        fetch('https://www.pivotaltracker.com/services/v5/projects/' + project.pivopid + '/memberships?token=' + process.env.pivotoken, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((res) => res.json())
            .then((pivomembers) => {
                res.render('view', {
                    page: 'Project view',
                    projects: project,
                    pivomembers: pivomembers
                })
            })
    });
});

router.post('/:id/view' ,ensureAuthenticated, (req, res) => {
    Project.getProjectById(req.params.id, function (err, project) {
        if (err) {
            throw err;
        }
        // new member invitation Pivotal Tracker
        fetch('https://www.pivotaltracker.com/services/v5/projects/' + project.pivopid + '/memberships?token=' + process.env.pivotoken, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: req.body.newmember,
                role: req.body.pivotalrole
            })
        });

        // new member invitation Bitbucket
        fetch('https://api.bitbucket.org/1.0/invitations/' + process.env.bitteam + '/' + project.projectname + '/', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64')
            },
            body: JSON.stringify({
                email: req.body.newmember,
                permission: req.body.bitpermvalue
            }),
            timeout: 1000,
        });
    });
    //Redirect to Project page
    res.writeHead(301,
        {Location: 'view'}
    );
    res.end();
});

/*router.delete('/:id/view/pivomemberdelete', (req, res) => {
    Project.getProjectById(req.params.id, function (err, project) {
        if (err) {
            throw err;
        }

        fetch('https://www.pivotaltracker.com/services/v5/projects/' + project.pivopid + '/memberships/' + req.body.pivomemberidfordelete.value + '?token=' + process.env.pivotoken, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }
        });

        //Redirect to Project page
        res.writeHead(301,
            {Location: 'view'}
        );
        res.end();
    })
});*/

router.post('/:id/deploy', (req, res) => {
    Project.getProjectById(req.params.id, function (err, project) {
        if (err) {
            throw err;
        }

        var conn = new Client();
        conn.on('ready', function () {
            console.log(project.projectname + 'Database drop and dump');
            conn.exec('mysqldump -u cdae ' + project.dbname + ' > /home/cdae/mysqldump/' + project.dbname + 'dump.sql ', function (err, stream) {
                conn.exec('mysql -Nse \'show tables\' ' + project.dbname + ' | while read table; do mysql -e "drop table $table" ' + project.dbname + '; done', function (err, stream) {
                    if (err) throw err;
                    stream.on('close', function (code, signal) {
                        console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                        conn.end();
                    }).on('data', function (data) {
                        console.log('STDOUT: ' + data);
                    }).stderr.on('data', function (data) {
                        console.log('STDERR: ' + data);
                    });
                })
            });
        }).connect({
            host: project.sshhost,
            port: 22,
            username: project.sshusername,
            privateKey: require('fs-extra').readFileSync('.ssh/id_rsa')
        });

        execSh('capsulejs deploy prod --config data/' + project.projectname + '.json', function (err) {
            if (err) {
                console.log("Exit code: ", err.code);
            }
            return;

        });

        fetch('https://api.bitbucket.org/2.0/repositories/' + process.env.bitteam + '/' + project.projectname + '/commits', {
            method: 'GET',
            headers: {
                'Authorization': 'Basic ' + new Buffer(process.env.bituser + ':' + process.env.bittoken).toString('base64')
            }
        }).then((res) => res.json())
            .then((data) => {
                fetch('https://www.pivotaltracker.com/services/v5/source_commits?token=' + pivotoken, {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        source_commit: {
                            commit_id: data.values[0].hash,
                            url: 'https://bitbucket.org/' + process.env.bitteam + '/' + project.projectname + '/commits/' + data.values[0].hash,
                            message: data.values[0].message,
                            author: data.values[0].author.raw,
                            repo: project.projectname
                        }
                    })
                }).then((res) => res.json())
                    .then((data2) => {
                        fetch('https://www.pivotaltracker.com/services/v5/projects/' + project.pivopid + '/stories/' + data2[0].id + '?token=' + pivotoken, {
                            method: 'PUT',
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({
                                current_state: "delivered"
                            })

                        })
                    })

                //console.log(data.values[0].author.raw);
                async function main() {

                    let transporter = nodemailer.createTransport({
                        host: process.env.mailhost,
                        port: process.env.mailport,
                        secure: true, // true for 465, false for other ports
                        auth: {
                            user: process.env.mailuser, // generated ethereal user
                            pass: process.env.mailpassword // generated ethereal password
                        }
                    });

                    // send mail with defined transport object
                    let info = await transporter.sendMail({
                        from: '"Automated message from Continuous Deployment Admin Environment" cdae@tonismals.ee', // sender address
                        to: process.env.devmail, // list of receivers
                        subject: "New deployment for " + project.projectname + ' is done', // Subject line
                        text: 'Project ' + project.projectname + ' is updated, repository link is here: https://bitbucket.org/' + process.env.bitteam + '/' + project.projectname + '/src\n' +
                            '\n' +
                            'Latest commit is:' + data.values[0].message + 'by ' + data.values[0].author.raw + '\n' +
                            'Latest changes you can check from here: https://bitbucket.org/' + process.env.bitteam + '/' + project.projectname + '/commits/' + data.values[0].hash + '\n' +
                            '\n' +
                            '\n' +
                            'This is automated message from deployment'
                    });

                    console.log("Message sent: %s", info.messageId);
                    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

                }

                main().catch(console.error);
            });


        async function main() {

            let transporter = nodemailer.createTransport({
                host: process.env.mailhost,
                port: process.env.mailport,
                secure: true, // true for 465, false for other ports
                auth: {
                    user: process.env.mailuser, // generated ethereal user
                    pass: process.env.mailpassword // generated ethereal password
                }
            });

            // send mail with defined transport object
            let info = await transporter.sendMail({
                from: '"Automated message from Continuous Deployment Admin Environment" cdae@tonismals.ee', // sender address
                to: project.productowner, // list of receivers
                subject: "New version of product is deployed", // Subject line
                text: 'Hello\n' +
                    '\n' +
                    '\n' +
                    'New version of product is deployed \n' +
                    '\n' +
                    'You can check new version of page here: http://' + project.sshhost + '/' + project.projectname + '\n' +
                    '\n' +
                    'Story in ready to approve here: https://www.pivotaltracker.com/n/projects/' + project.pivopid + '\n' +
                    '\n' +
                    '\n' +
                    '\n' +
                    '\n' +
                    'This is automated message from deployment'
            });

            console.log("Message sent: %s", info.messageId);

        }

        main().catch(console.error);


        //res.redirect('/');
        /*res.json(project.projectname);*/

    });
    res.end();
});


module.exports = router;
