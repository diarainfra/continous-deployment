var express = require('express');
const app = express();
var router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');

// define the home page route
router.get('/', ensureAuthenticated, (req, res) => {
    res.render('home', {page:'Home'})
});



module.exports = router;
