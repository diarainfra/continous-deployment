var request = require('supertest');
var server = request.agent('http://localhost:8000');

describe('Login panel', function(){
    it('login', loginUser());
    it('should user can be log into software', function(done){
        server
            .get('/')
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
});

function loginUser() {
    return function(done) {
        server
            .post('/users/login')
            .send({ username: 'fortesting', password: 'fortesting' })
            .expect(302)
            .expect('Location', '/')
            .end(onResponse);

        function onResponse(err, res) {
            if (err) return done(err);
            return done();
        }
    };
};
