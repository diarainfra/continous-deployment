const express = require('express');
const path = require("path");
//const expressLayouts = require('express-ejs-layouts');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const app = express();
const bodyParser = require('body-parser');
const logger = require('morgan');
const dotenv = require('dotenv');
dotenv.config();



// Set the view engine to ejs

app.set('view engine', 'ejs');

// Passport Config
require('./config/passport')(passport);

// Express session
app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
app.use(logger('dev'));

// Connect flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});

app.use(express.static(__dirname + '/public'));


app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.disable('x-powered-by');



var login = require('./routes/login');
var index = require('./routes/index');
var projects = require('./routes/projects');
var newproject = require('./routes/newproject');
var register = require('./routes/users');
var newserver = require('./routes/newserver');


app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, text/html, authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/login', login);
app.use('/users', register);
app.use('/', index);
app.use('/projects', projects);
app.use('/projects/new', newproject);
app.use('/projects/newserver', newserver);

/*// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = res.redirect("/");
    next(err);
});*/


//Server running Line
app.listen(process.env.PORT, () => console.log(`Example app listening on port ${process.env.PORT}!`));

